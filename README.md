# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone <link do repozitorija>
```

Naloga 6.2.3:
https://bitbucket.org/creativ10/stroboskop/commits/a75045f32d9e7a29bc72ad274927f1bcd5102bab

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/creativ10/stroboskop/commits/b4d789f55dd5eadd3e52c6122b300503eaf1dbda

Naloga 6.3.2:
https://bitbucket.org/creativ10/stroboskop/commits/3b346486705bb82ad31179e43296930ffb1b0f16
Naloga 6.3.3:
https://bitbucket.org/creativ10/stroboskop/commits/ea6bd1bc1303ac8f83838a216c4ee6083d55eac6

Naloga 6.3.4:
https://bitbucket.org/creativ10/stroboskop/commits/9ff8f596f9469758448d55e5ef38f5f1e79a1093

Naloga 6.3.5:

```
git checkout master
git merge izgled 
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/creativ10/stroboskop/commits/4e789e140f30f3025452a5b5bd2d51a65328f6a3

Naloga 6.4.2:
https://bitbucket.org/creativ10/stroboskop/commits/3196d9655427d5c62b9c273a97ceb5f97382c4a5

Naloga 6.4.3:
https://bitbucket.org/creativ10/stroboskop/commits/66d0ef6f1b340f134f971b373549e2e5ea77b292

Naloga 6.4.4:
https://bitbucket.org/creativ10/stroboskop/commits/073c0f2b7eded61f4da43ff4f4997ad916e5ee29